import random

#while loop
#repeat loop
#for loop

name = input("What is your name?")
print(f"Hello {name}! I am going to try to guess your birthday, please give me three tries!")




#word_month = ["Jan", "Feb", "March", "April", "May"]

while True:
    
    month = random.randint(1,12)
    year = random.randint(1920,2022)

    print(f"Is your birthday {month,year}?")

    answer = input("Yes or No ")
    if answer == "Yes":
        print("I knew it!")
        exit()
    elif answer == "No":
        print("Drat! lemme try again")
        continue
    else:
        print("please answer my question again")
