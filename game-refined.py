import random

#create a birthday guessing game
#the computer will guess a birthday, and gets 5 tries
#the user will input if the computer is correct
#there are conditions if the computer is right, wrong, or exceeds 5 tries
#I need to use for and range function
#the loop will run the program at least 5 times to try guess the user's bday

def month_converter(month_number):
    month_list = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    return month_list[month_number-1]

name = input("What is your name? ")
print(f"Hello {name}! I am going to try to guess your birthday, please give me at least 5 tries!")

#month_list = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

for tries in range(1,6):
    
    month = month_converter(random.randint(1,12))
    year = random.randint(2022-100,2022)

    print(f"This will be guess #{tries}. Is your birthday in {month} {year}?")

    answer = input("Yes or No ")
    
    if answer == "Yes":
        print("I knew it!")
        exit()
    elif tries < 4:
        print("Drat! Lemme try again!")
        continue
    elif tries == 5:
        print("I have other things to do. Good bye.")
  
        
